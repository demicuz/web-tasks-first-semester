# Lab 0

## Регистрации

1. Зарегистрироваться fit.mospolytech.ru
1. Отправить завку на сервер https://fit.mospolytech.ru/systems/servers


- Зарегистрироваться figma.com
- Зарегистрироваться gitlab.com

## Установка ПО

- Установить OpenVPN (VPN)
- Установить FileZilla (FTP)
- Установить Putty (SSH)
- Установить Git
- Установить VS Code или любой другой

## Базовые команды

Команды git (add, commit, init, clone, push...)
Команды терминала (cd, mkdir, cp, rm, pwd...)

## Проба пера

1. Создать проект на gitlab
1. Склонировать его
1. Создать index.html
    1. header, nav, main, section, footer, h1, h2, p, a, img, ul/ol, li
1. Проверить на валидаторе
1. Сохранить изменения
1. Сделать коммит
1. Отправить коммит на удаленный репозиторий
