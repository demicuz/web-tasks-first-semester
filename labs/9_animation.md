# Лабораторная работа №8

Фон, градиент, тени, переходы, преобразования, анимации

**Цель:** изучить свойства css для создания фонов, градиентов, теней, переходов, преобразований и анимаций, овладеть навыками создания на странице анимаций (animation), переходов (transition) и преобразований (transition). 

## Содержание и порядок выполнения лабораторной работы:

1. Подготовка к выполнению лабораторной работы изучить:

- background:
    - background-attachment;
    - background-clip;
    - background-color;
    - background-image;
    - background-position;
    - background-repeat;
    - background-size;
- gradient:
    - linear;
    - radial;
    - conic;
- shadow:
    - text;
    - box;
- transform;
- transition;
- animation и keyframes;

и примеры их использования.

2. Для выполнения работы необходимо:
    - для элементов, у которых есть фоновые изображения добавить цвет (background-color);
    - для элементов, у которых заданы стили с псевдоклассом :hover добавить плавное изменение свойств через transition;
    - в макет на главной странице (index.html) в любую секцию с карточками, добавить новую дополнительную карточку. Сначала в карточке должен отображаться анимированный указатель загрузки (loader), который спустя несколько секунд становится прозрачным и после отображается содержимое карточки.
    - на странице с формой добавить использование теней;
    - создать третью страницу (demo.html) на которую добавить примеры:
        - Линейный, радиальный, конический градиенты (по 1 примеру);
        - 2D-преобразования (2 примера);
        - 3D-преобразования (2 примера);
        - Переходы transition (2 примера).

3. Зафиксировать результаты работы с помощью системы контроля версий git и отправить репозиторий на GitLab/mospolytech_web.
Загрузить проект на сервер через sftp или используя ssh.

Требования к html и css:

- Удобочитаемый код (Руководство по оформлению HTML/CSS кода от Google);
- Корректное использование тегов.
- Отсутствие ошибок на валидаторе для html и css;

## Результаты выполнения лабораторной работы:

Статические веб-страницы на хостинге, со структурой и оформлением, которые соответствует требованиям и сохранены в системе контроля версий.

## Источники

- [doka.guide](https://doka.guide/css/)

### Фоны

- [Background](https://www.freecodecamp.org/news/learn-css-background-properties/)

### Градиенты

- [Все о градиентах](https://css-tricks.com/css3-gradients/)
- [MDN: linear-gradient()](https://developer.mozilla.org/en-US/docs/Web/CSS/gradient/linear-gradient())
- [MDN: radial-gradient()](https://developer.mozilla.org/en-US/docs/Web/CSS/gradient/radial-gradient())

### Тени

- [Создание теней](https://keyframes.app/shadows/)

### Переходы

- [CSS transitions](https://www.joshwcomeau.com/animation/css-transitions/)

### Анимации

- [Doka](https://doka.guide/css/animation/)
- [MDN](https://developer.mozilla.org/ru/docs/Web/CSS/animation)
- [Создание ключевых кадров](https://keyframes.app/animate/)

### Дополнительные

- [5 Awesome CSS Animation Resources for All Web Developers](https://blog.devgenius.io/5-awesome-css-animation-resources-for-all-web-developers-70bca71a5e1e)
- [Пример карточек](https://codepen.io/Jhonierpc/pen/MWgBJpy)
