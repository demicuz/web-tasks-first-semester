# Лабораторная работа №5

Изображения

**Цель:** изучить цветовые пространства, форматы изображений, получить первоначальные навыки оптимизации изображений и правильное использование тегов.

## Содержание и порядок выполнения лабораторной работы:

1. Подготовка к выполнению лабораторной работы:

- изучить основы цвета;
- изучить форматы изображений:
    - jpeg,
    - png,
    - gif,
    - webp,
    - svg;
- изучить сценарии использования форматов изображений;
- изучить теги и атрибуты касающиеся изображений:
    - img,
    - picture,
    - figure,
    - figcaption,
    - svg,
    - width,
    - height;
- изучить свойства для замещаегого контента:
    - object-fit,
    - object-position;

2. Для выполнения работы необходимо:

- оптимизировать размеры и сжатия изображений;

- в index.html и style.css из четвертой работы:
    - использовать корректные теги для изображений (img или figure);
    - использовать подходящие форматы изображений (jpg, png, svg);
    - при необходимости использовать object-fit или object-position;

3. Зафиксировать результаты работы с помощью системы контроля версий git и отправить репозиторий на GitLab/mospolytech_web.
Загрузить проект на сервер через sftp или используя ssh.

Требования к html и css:

- Удобочитаемый код (Руководство по оформлению HTML/CSS кода от Google);
- Корректное использование тегов.
- Отсутствие ошибок на валидаторе для html и css;

## Результаты выполнения лабораторной работы:

Статическая веб-страница на хостинге, со структурой и оформлением, которая соответствует требованиям и сохранена в системе контроля версий.

## Дополнительные источники

`<img>` - https://developer.mozilla.org/ru/docs/Web/HTML/Element/img

`<picture>` - https://developer.mozilla.org/ru/docs/Web/HTML/Element/picture

`<figure>` - https://developer.mozilla.org/ru/docs/Web/HTML/Element/figure

object-fit - https://developer.mozilla.org/ru/docs/Web/CSS/object-fit

object-position - https://developer.mozilla.org/ru/docs/Web/CSS/object-position

Оптимизация изображений - squoosh.app
